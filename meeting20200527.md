# BlankOn

## Waktu

Rabu, 27 Mei 2020, 19:00 s.d. 20:00 WIB

## Lokasi

Daring melalui Google Meet

## Peserta

- @estu
- @samsulmaarif
- @benben159
- @misskecupbung
- @aftian
- @yht1
- Dzulkifli

## Agenda

- bahas isu terkini terkait mesin dan 
- status pengembangan
- pembagian tugas


## List mesin

* Waljinah | sby network, sumbangan, HDD punya blankon
  - dns, web
  - mesin prihatin,
* alynne, sakti, offline | g punya sponsor resmi. worker
* rani, radnet | mesin muka | lumbung.

-------------------------------------------------
## Mesin baru

- waljinah upgrade mesin dan benwidh, (WIP) setelah Corona


## Action Item

- Butuh web panduan idup
  - panduan login pake aku.boi. <-- ini out of service
  - cara paling gampang, lepas aku. wordpress nya pake login.
  - wp upgrade, g bisa kalo masih pake aku.
  - login, sso, selain akun wp, coba cari lain.
- aku jalan di docker, PIC, YHT & Estu
- Pengembangan
  - CI/CD | gitlab 
  - service irgsh, run docker, PIC pak samsul
  - servernya pake kubernetes, pake akun pak piko (GCP)
  - backup server, alicloud estu
  - output irgsh simpen di? > buat list paket.
  - riset pengembangan dengan kiwi untuk build iso
